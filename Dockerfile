FROM alpine:edge
ENTRYPOINT ["/entrypoint.sh"]
RUN apk add --no-cache \
	ansible \
	bash \
	ca-certificates \
	dumb-init \
	openssh-client \
	openssl \
	py3-crypto \
	rsync \
	sshpass \
	&& {\
	  echo '#!/usr/bin/dumb-init /bin/bash';\
	  echo 'if [[ -n "$DEBUG" ]]; then set -x ; trap "sleep 3600" EXIT; fi';\
	  echo 'last="${@: -1}"';\
	  echo 'if [[ "${last##*.}" =~ ya?ml ]]; then set -- ansible-playbook "$@"; else set -- ansible "$@"; fi';\
	  echo 'exec "$@"';\
	} >/entrypoint.sh \
	&& chmod 755 /entrypoint.sh
ENV ANSIBLE_HOST_KEY_CHECKING=False
